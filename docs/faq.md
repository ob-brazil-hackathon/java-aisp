# FAQ

## User Authentication & Authorisation

### 1. How is user authentication done
FaaS uses OAuth2.0 & OpenID Connect for the authentication. We support Username/Password and Google Authenticator (other methods can also be supported)

### 2. What type of user authentication is provided
* `Developer Portal User` - it could another business team who want to consume your API product by creating apps
* `Enduser` - Enduser of your fintech application
* `Application` - OAuth client for consuming APIs

### 3. Where can I find the FaaS OAuth2 Wellknown Configuration endpoint
`#TODO needs update` - Each Fintech will have their own endpoint. The URI is <https://localhost>

### 4. How can I access userinfo on my API
The primary source is `access_token` and if needed we can provide `id_token` to share the information of the user. Your API will receive the Authorisation header.

### 5. Can I carry on local development without authentication (access_token) validation?
Yes, of course. You can custom code a JWT or use expired token which contains all the claims you need for your local development.

### 6. Can I use different role or group for the user?
Our system support such capability but need to be done with help of Banfico Techncial Team. Currently we support `#TODO needs update`

## API Products

### 7. How can I publish the my API product?
You need to login to API Manager and publish the product using Swagger/RAML files

### 8. If the code changes, do I need to republish the API Specification?
Yes, our API manager doesn't auto update the API specification for the consumer of the API. It need to be manually done by login into API Manager. In future we may support auto-publish new API specification

## Consuming OBAF API Product

### 9. What is OBAF?
It is `Open Banking Aggregation Framework`. It helps to aggregate Open Banking AIS APIs from different banks. Currently we support our [OBIE Bank](https://developer.obiebank.banfico.com/) & [AutBank](https://developer.autbank.banfico.com/)

### 10. How can I consume OBAF from my API Product?
Login into FaaS developer portal and create an App for OBAF. Using the Client-id and Client-Secret & based on OAuth2.0 Client Credentials grant, authenticate the App and get `access_token` to access `OBAF API Product`