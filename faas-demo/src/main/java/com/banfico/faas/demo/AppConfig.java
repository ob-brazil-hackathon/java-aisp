
package com.banfico.faas.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class AppConfig {

	@Value("${aggregator.host}")
	private String aggregatorUrl;

	@Value("${aggregator.api.token.endpoint}")
	private String aggregatorApiTokenEndpoint;

	@Value("${aggregator.app.id}")
	private String aggregatorAppId;

	@Value("${aggregator.app.pass}")
	private String aggregatorAppPass;

	public String getAggregatorUrl() {
		return aggregatorUrl;
	}

	public void setAggregatorUrl(String aggregatorUrl) {
		this.aggregatorUrl = aggregatorUrl;
	}

	public String getAggregatorAppId() {
		return aggregatorAppId;
	}

	public void setAggregatorAppId(String aggregatorAppId) {
		this.aggregatorAppId = aggregatorAppId;
	}

	public String getAggregatorAppPass() {
		return aggregatorAppPass;
	}

	public void setAggregatorAppPass(String aggregatorAppPass) {
		this.aggregatorAppPass = aggregatorAppPass;
	}

	public String getAggregatorApiTokenEndpoint() {
		return aggregatorApiTokenEndpoint;
	}

	public void setAggregatorApiTokenEndpoint(String aggregatorApiTokenEndpoint) {
		this.aggregatorApiTokenEndpoint = aggregatorApiTokenEndpoint;
	}

}