/*
 * OpenBanking Orchestrator API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.banfico.faas.demo.openapi.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.banfico.faas.demo.openapi.client.model.ServiceFormModelUi;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * ComBanficoOpenbankingOrchestratorControllerModelProviderGrantAccessForm
 */
@JsonPropertyOrder({
  ControllerModelProviderGrantAccessForm.JSON_PROPERTY_GRANT_ACCESS
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-12-03T14:47:16.599+05:30[Asia/Kolkata]")
public class ControllerModelProviderGrantAccessForm {
  public static final String JSON_PROPERTY_GRANT_ACCESS = "grantAccess";
  private ServiceFormModelUi grantAccess;


  public ControllerModelProviderGrantAccessForm grantAccess(ServiceFormModelUi grantAccess) {
    
    this.grantAccess = grantAccess;
    return this;
  }

   /**
   * Get grantAccess
   * @return grantAccess
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")
  @JsonProperty(JSON_PROPERTY_GRANT_ACCESS)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public ServiceFormModelUi getGrantAccess() {
    return grantAccess;
  }


  public void setGrantAccess(ServiceFormModelUi grantAccess) {
    this.grantAccess = grantAccess;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ControllerModelProviderGrantAccessForm comBanficoOpenbankingOrchestratorControllerModelProviderGrantAccessForm = (ControllerModelProviderGrantAccessForm) o;
    return Objects.equals(this.grantAccess, comBanficoOpenbankingOrchestratorControllerModelProviderGrantAccessForm.grantAccess);
  }

  @Override
  public int hashCode() {
    return Objects.hash(grantAccess);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ComBanficoOpenbankingOrchestratorControllerModelProviderGrantAccessForm {\n");
    sb.append("    grantAccess: ").append(toIndentedString(grantAccess)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

