/*
 * OpenBanking Orchestrator API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.banfico.faas.demo.openapi.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.banfico.faas.demo.openapi.client.model.ServiceFormModelJsonSchema;
import com.banfico.faas.demo.openapi.client.model.ServiceFormModelUiSchema;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * ComBanficoOpenbankingOrchestratorServiceFormModelUi
 */
@JsonPropertyOrder({
  ServiceFormModelUi.JSON_PROPERTY_JSON_SCHEMA,
  ServiceFormModelUi.JSON_PROPERTY_UI_SCHEMA
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-12-03T14:47:16.599+05:30[Asia/Kolkata]")
public class ServiceFormModelUi {
  public static final String JSON_PROPERTY_JSON_SCHEMA = "jsonSchema";
  private ServiceFormModelJsonSchema jsonSchema;

  public static final String JSON_PROPERTY_UI_SCHEMA = "uiSchema";
  private ServiceFormModelUiSchema uiSchema;


  public ServiceFormModelUi jsonSchema(ServiceFormModelJsonSchema jsonSchema) {
    
    this.jsonSchema = jsonSchema;
    return this;
  }

   /**
   * Get jsonSchema
   * @return jsonSchema
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")
  @JsonProperty(JSON_PROPERTY_JSON_SCHEMA)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public ServiceFormModelJsonSchema getJsonSchema() {
    return jsonSchema;
  }


  public void setJsonSchema(ServiceFormModelJsonSchema jsonSchema) {
    this.jsonSchema = jsonSchema;
  }


  public ServiceFormModelUi uiSchema(ServiceFormModelUiSchema uiSchema) {
    
    this.uiSchema = uiSchema;
    return this;
  }

   /**
   * Get uiSchema
   * @return uiSchema
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")
  @JsonProperty(JSON_PROPERTY_UI_SCHEMA)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public ServiceFormModelUiSchema getUiSchema() {
    return uiSchema;
  }


  public void setUiSchema(ServiceFormModelUiSchema uiSchema) {
    this.uiSchema = uiSchema;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ServiceFormModelUi comBanficoOpenbankingOrchestratorServiceFormModelUi = (ServiceFormModelUi) o;
    return Objects.equals(this.jsonSchema, comBanficoOpenbankingOrchestratorServiceFormModelUi.jsonSchema) &&
        Objects.equals(this.uiSchema, comBanficoOpenbankingOrchestratorServiceFormModelUi.uiSchema);
  }

  @Override
  public int hashCode() {
    return Objects.hash(jsonSchema, uiSchema);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ComBanficoOpenbankingOrchestratorServiceFormModelUi {\n");
    sb.append("    jsonSchema: ").append(toIndentedString(jsonSchema)).append("\n");
    sb.append("    uiSchema: ").append(toIndentedString(uiSchema)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

